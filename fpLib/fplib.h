
#define ACCEPT  1
#define DROP    2
#define MODIFY  3


#define FPLIB           "Libfp.so"
#define FPLIB_TEST      "./Libfp.so"
#define FP_WRITE_API    "fpWrite"
#define FP_READ_API     "fpRead"
#define FP_MSG_LEN      64

#define ETH_ALEN        6

typedef enum FpAction{
    fpAccept = ACCEPT,
    fpDrop = DROP,
    fpModify = MODIFY
} FpAction;

typedef struct NIC_STATUS {
    uint8_t *mac;
    uint8_t *ip;
} NIC_STATUS;


#define FP_WRITE_FUNC(x)    (*x)(uint8_t*,int,NIC_STATUS*,uint8_t*)
#define FP_READ_FUNC(x)     (*x)(uint8_t*,int,NIC_STATUS*,uint8_t*)

