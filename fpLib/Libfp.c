#include <stdio.h>
#include <stdint.h>

static void hex_dump(FILE *f, const uint8_t *buf, int size)
{
	int len, i, j, c;

	for(i=0;i<size;i+=16) {
		len = size - i;
		if (len > 16)
			len = 16;
		fprintf(f, "%08x ", i);
		for(j=0;j<16;j++) {
			if (j < len)
				fprintf(f, " %02x", buf[i+j]);
			else
				fprintf(f, "   ");
		}
		fprintf(f, " ");
		for(j=0;j<len;j++) {
			c = buf[i+j];
			if (c < ' ' || c > '~')
				c = '.';
			fprintf(f, "%c", c);
		}
		fprintf(f, "\n");
	}
}

void filterProcess(uint8_t *mem, int size)
{
	fprintf(stdout,"Get packet send from guest\n");
	hex_dump(stdout,mem,size);
}
