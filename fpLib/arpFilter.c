#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <net/ethernet.h>
#include "fplib.h"

static void hex_dump(FILE *f, const uint8_t *buf, int size)
{
	int len, i, j, c;

	for(i=0;i<size;i+=16) {
		len = size - i;
		if (len > 16)
			len = 16;
		fprintf(f, "%08x ", i);
		for(j=0;j<16;j++) {
			if (j < len)
				fprintf(f, " %02x", buf[i+j]);
			else
				fprintf(f, "   ");
		}
		fprintf(f, " ");
		for(j=0;j<len;j++) {
			c = buf[i+j];
			if (c < ' ' || c > '~')
				c = '.';
			fprintf(f, "%c", c);
		}
		fprintf(f, "\n");
	}
}


void filterProcess(uint8_t *mem, int size)
{
	fprintf(stdout,"Get packet send from guest\n");
	hex_dump(stdout,mem,size);
}

FpAction fpWrite(uint8_t *mem, int size, NIC_STATUS *nic, uint8_t *msg)
{
    struct ether_header *eth = (struct ether_header*)mem;
   
    /*
    printf("Source MAC address:\n");
    hex_dump(stdout,eth->ether_shost,ETH_ALEN);
    printf("Physical MAC address:\n");
    hex_dump(stdout,nic->mac,ETH_ALEN);
    printf("Source IP address:\t%d.%d.%d.%d\n",nic->ip[0],nic->ip[1],nic->ip[2],nic->ip[3]);
    */
    
    if (memcmp(eth->ether_shost, nic->mac, ETH_ALEN) != 0){
        if (msg != NULL){
            memcpy(msg,"Drop illegal MAC address from VNIC\0",FP_MSG_LEN);
        }
        return DROP;
    }

    return ACCEPT;
}

FpAction fpRead(uint8_t *mem, int size, NIC_STATUS *nic, uint8_t *msg)
{
    return ACCEPT;
}

