<html>
    <head>
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="js/jquery-1.7.2.min.js" type="text/javascript" ></script>
        <script src="js/core.js" type="text/javascript" ></script>
        <script src="js/ajaxfileupload.js" type="text/javascript" ></script>
        <script class="jsbin" src="js/jquery.dataTables.nightly.js"></script>
</head>

    <body>
         <!-- outline --!>
        <div id="outline">
            <div id="title" align="center">
                <h1>Centralized Command and Control Center</h1>
            </div>
        </div>

        <!-- content --!>
        <div id="content">
            
            <div id="caption" align="center">
                <table border="0" id="nav">
                    <tr>
                        <th>
                            <a id="nav-tag" href="#about" onClick="LoadHTML('about.txt')">About</a>
                        </th>
                        <th>
                            <a  id="nav-tag" href="#monitor" onClick="LoadHTML('monitor.php')">Monitor</a>
                        </th>
                        <th>
                            <a  id="nav-tag" href="#Log" onClick="LoadHTML('showLog.php')">Log</a>
                        </th>
                        <th>
                            <a  id="nav-tag" href="#deploy" onClick="LoadHTML('deploy.php')">Deploy</a>
                        </th>
                    </tr>
                </table>
            </div>
            
            <div id="display" align="center">
        <script>LoadHTML('monitor.php');</script>
            </div>
        </div>

         <!-- footer --!>
         <div id="footer">
         </div>
    </body>
</html>
