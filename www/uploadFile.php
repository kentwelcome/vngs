<?php

function copyFileTo($src,$dst)
{
    $cmd = "cp $src $dst";
    system($cmd);
}

function buildUploadFile()
{
    return exec("gcc -fPIC -shared -o tmp/test.so tmp/test.c 2> /dev/stdout");
}

function build64()
{
    return exec("gcc -m64 -fPIC -shared -o tmp/test.so tmp/test.c 2> /dev/stdout");
}

function unitTestTheUploadFile()
{
    return exec("./tmp/UnitTest");
}


function updateToDb($name,$note)
{
    include("mysql.inc.php");   
    $tblFp = "FilteringProcess";
    $sql = "select * from $tblFp where name = '$name'";
    $result = mysql_query($sql);
    $fid = 0;

    if ($row = mysql_fetch_array($result)){
        $fid = $row['fid'];    
    }

    if ($fid > 0){ // update
       $sql = "update FilteringProcess set note = '$note', uptime=CURRENT_TIMESTAMP where fid=$fid"; 
    } else { // insert
       $sql = "INSERT INTO `vngs`.`FilteringProcess` (`fid`, `name`, `uptime`, `note`) VALUES (NULL, '$name', CURRENT_TIMESTAMP, '$note');"; 
    }
    $return = mysql_query($sql);
    mysql_close();
}


if ($_POST['Note']){
    $fpNote = $_POST['Note'];   
} else {
    $fpNote = "No comment";    
}

if ($_FILES["file"]["error"] > 0)
{
    $fileJson = array(
            "status" => "error", 
            "error" => $_FILES["file"]["error"]
            );

    echo json_encode($fileJson);
}
else
{
    $fileType = pathinfo($_FILES["file"]["name"],PATHINFO_EXTENSION);
    
    $fileJson = array(
            "status" => "ok",
            "upload" => $_FILES["file"]["name"],
            "type"  => $fileType,
            "size"  => ($_FILES["file"]["size"]/1024),
            "stored" => $_FILES["file"]["tmp_name"]
            );


    if($fileType != "so" ){
        move_uploaded_file($_FILES["file"]["tmp_name"],"tmp/test.c");

        // Compile the source code as test.so
        $gcc = buildUploadFile();

        // Run unit test 
        if($gcc == ""){
            $unit = unitTestTheUploadFile();
        }

        if ($unit == "pass"){
            $fpName = pathinfo($_FILES["file"]["name"],PATHINFO_FILENAME).".so";
            $fpSource = pathinfo($_FILES["file"]["name"],PATHINFO_FILENAME).".c";
            $newPath = "./fpDir/".$fpName;
            $newSource = "./fpDir/".$fpSource;
            copyFileTo("./tmp/test.so",$newPath);
            copyFileTo("./tmp/test.c",$newSource);
            updateToDb($fpName,$fpNote);
        }
    } else {
        move_uploaded_file($_FILES["file"]["tmp_name"],"tmp/test.so");
        $gcc = "";    
        $unit = unitTestTheUploadFile();
        
        if ($unit == "pass"){
            $fpName = pathinfo($_FILES["file"]["name"],PATHINFO_FILENAME).".so";
            $newPath = "./fpDir/".$fpName;
            copyFileTo("./tmp/test.so",$newPath);
            updateToDb($fpName,$fpNote);
        }
    }

    $fileJson['gccError'] = $gcc;
    $fileJson['unitTest'] = $unit;

    echo json_encode($fileJson);
    exec("rm -f tmp/test.so tmp/test.c");
}
?>
