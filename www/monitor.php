<div id="HostList" >
    <h2>Host Machine List</h2>
    <table align="center">
        <thead class="headerTable">
            <tr>
                <th>ID</th>
                <th>IP</th>
                <th>VM Num</th>
            </tr>
        </thead>
        <tbody class="dataTable">
<?php
include("mysql.inc.php");
$tblHost="HostMachine";    
$tblVM  ="VirtualMachine";
$sql    = "select * from $tblHost";
$result = mysql_query($sql);

for ($i = 0 ;$row[$i] = mysql_fetch_array($result); $i++){

    $hid = $row[$i]['HID'];
    print "<tr class='tr-out' onmouseover=this.className='tr-over' onmouseout=this.className='tr-our' onClick='$(\"#VmList\").load(\"VmStatus.php\",{hid: $hid});'>\n";
    print "<th>".$row[$i]['HID']."</th>";
    print "<th>".$row[$i]['IP']."</th>";
    print "<th>".$row[$i]['VmNum']."</th>";
    print "</tr>\n";
}
?>
        </tbody>
    </table>

    <h2>VM Status</h2>
    <div id="VmList">
        <table align="center">
           <thead class="headerTable">
                <tr>
                    <th>IP</th>
                    <th>MAC Address</th>
                    <th>Device</th>
                </tr>   
            </thead>
        </table>
    </div>
</div>
<div id="FpLog">
    <h2>Filter Process Log</h2>
    <table id="log" align="center">
        <thead class="headerTable">
            <tr>
                <th>Time</th>
                <th>Type</th>
                <th>Action</th>
                <th>Message</th>
            </tr>
        </thead>
    </table>
</div>
