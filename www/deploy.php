<h2>Deploy Filtering Process</h2>

<script>

function useFilter(fid){
    $.ajax({
        url: 'useFp.php',
        type: 'POST',
        data: {
            "fid": String(fid)
        },
        error: function(xhr) {
        },
        success: function(response) {
        }
    });
}

function ajaxFileUpload(){
    
    //starting setting some animation when the ajax starts and completes
    $("#loading")
        .ajaxStart(function(){
            $(this).show();
        })
            .ajaxComplete(function(){
                $(this).hide();
            });


    //$("div#upDisplay").html("<h2>"+$("#fileupload").val()+"</h2>"); 

    var fileName = $("#fileupload").val();
    var fpNote = $("#fpNote").val();

    $.ajaxFileUpload({
        url:'uploadFile.php',
        fileElementId:'fpLib',
        data: {Note: String(fpNote)} ,
        dataType: 'json',
        success: function (data, status, e){
            if (data.unitTest == "pass"){
                $("#upDisplay").append("<p>Upload Success</p>");
                $("#fpTable").load("fpTable.php");
            } else {
                $("#upDisplay").append("<p>Error!</p>");
                if (data.gccError == ""){
                    $("#upDisplay").append("<p>Can't Pass the Unit Test</p>");
                    $("#upDisplay").append("<p>"+ data.unitTest +"</p>");
                } else {
                    $("#upDisplay").append("<p>Compilation Error</p>");
                }

            }
        }
    }
                    );
}

$("#loading").hide();

//$("#fpTable").load("fpTable.php",{index: 0});
</script>

<div id="loading">
    </br>
    <img src='images/loading.gif' width="64" height="64" />
    <p style="color: #FFFFFF">Loading</p>
</div>

<table id="fpList" >
    <thead class="headerTable">
        <tr>
            <th>Name</th>
            <th>Updated Time</th>
            <th>Note</th>
            <th>Use</th>
        </tr>
    </thead>
    <tbody class="dataTable" id="fpTable">
    <?php
        include("fpTable.php");
    ?>
    </tbody>
</table>

<h2>Upload Filtering Process</h2>
<p>Support only *.c , *.cpp , *.so</p>
<form id="dropForm" action="" method="post" enctype="multipart/form-data">
<table>
    <tr>
        <th><label for="file">Filename:</label></th>
        <th><input id="fpLib" type="file" name="file"/></th> 
    </tr>
    <tr>
        <th><label>Note:</label></th>
        <th><textarea id="fpNote" rows="2" cols="32"></textarea></th>
    </tr>
</table>
<input type="button" value="Submit" name="send" onClick="ajaxFileUpload();">
</form>

<div id="upDisplay">
</div>

