#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <dlfcn.h>
#include "fplib.h"
#include <string.h>

void    *handle;
int     FP_WRITE_FUNC(fpWrite);
int     FP_READ_FUNC(fpRead);
char    *error;

int writeTest(int ans)
{
    fpWrite = dlsym(handle,FP_WRITE_API); 
    uint8_t mem[60] = {"\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc\xcc"};
    NIC_STATUS *nic = malloc(sizeof(NIC_STATUS));
    uint8_t mac[6] = {"\x54\x56\x00\x12\x34\x56"};
    uint8_t ip[4] = {"\x08\x08\x08\x08"};

    nic->mac = mac;
    nic->ip  = ip;

    if (ans == fpWrite(mem,60,nic,NULL)){
        return 1;
    } else {
        return 0;
    }
}

int readTest(int ans)
{
    return 0;
}

int main(int argc, char *argv[])
{
    handle = dlopen("./tmp/test.so",RTLD_NOW);
    if (handle == NULL){
        printf("Open library error: %s\n",dlerror());
        return -1;
    }

    fpWrite = dlsym(handle,FP_WRITE_API); 
    if (fpWrite == NULL){
        printf("Load function fpWrite error\n");
        return -1;
    }
    fpRead = dlsym(handle,FP_READ_API); 
    if (fpRead == NULL){
        printf("Load function fpRead error\n");
        return -1;
    }
    
    printf("pass");
    return 1;
}
