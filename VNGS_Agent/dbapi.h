#include <stdint.h>
#include "struct.h"

#define DBSERVER    "localhost"
#define USER        "vngs"
#define PASSWD      "islab@833"
#define DBSELECTED  "vngs"
#define CMD_LEN     128
#define FPLOG_LEN   512

// Table 
#define HM_TABLE    "HostMachine"
#define VM_TABLE    "VirtualMachine"

// Function

int registHostMachine(uint8_t *ip);
int registVirtualMachine(VmStatus *s,int hid,int pid);
int selectHidByIp(uint8_t *ip);
int selectVidByIp(uint8_t *ip, int hid);
int updateVirtualMachineStatus(VmStatus *s, int vid, int pid);
int vmDown(int vid, int hid);
int updateVmNumInHost(int hid);
int insertFpLog(FpStatus *fp, int vid, int hid);
int getFpModuleName(int vid, char *name);
