#include <stdint.h>
#define PIPE_READ_LEN   128
#define PIPE_WRITE_LEN   128

#define DEPLOY  0
#define CONNECTION 1

typedef struct DeployMsg {
    uint8_t type;
    uint8_t path[128];
    uint8_t name[64];
} DeployMsg;

// Function

char *pipeCmd(char *cmd);
int getNetworkStatusByMac(uint8_t *mac, uint8_t *ip, uint8_t *tap);
int fpDownloader(DeployMsg *fp);
