
#define     AGENT_NAME  "vngs-agent"

#define     ACCEPT  1
#define     DROP    2
#define     MODIFY  3

#define     WRITE   1
#define     READ    0

#define     NE2000  1
#define     VIRTIO  2

#define     FPSTATUS    0x01
#define     VMSTATUS    0x02

typedef struct RecvCmd {
    uint8_t cmd;
    uint8_t buf[127];
} RecvCmd;

typedef struct FpStatus {
    uint8_t  cmd;
    uint32_t time;
    uint8_t  type;
    uint8_t  phys[6];
    uint8_t  ip[4];
    uint8_t  action;
    uint8_t  msg[108];
} FpStatus;


typedef struct VmStatus {
    uint8_t cmd;
    uint8_t nic;
    uint8_t ip[17];
    uint8_t tap[8];
    uint8_t mac[18];
    uint8_t img[16];
    uint8_t msg[67];
} VmStatus;


typedef struct FpName {
    uint8_t name[64];
} FpName;
