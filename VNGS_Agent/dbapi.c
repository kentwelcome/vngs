#include <mysql.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "dbapi.h"


MYSQL *initDbConnection(MYSQL *conn)
{
    conn = mysql_init(NULL);
    if (!mysql_real_connect(conn, DBSERVER, USER, PASSWD, DBSELECTED, 0, NULL, 0)){
        fprintf(stderr, "%s\n", mysql_error(conn));
        return NULL;
    }

    return conn;
}

int selectHidByIp(uint8_t* ip)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    MYSQL_ROW   row;
    char cmd[128];
    int         ret = 0;

    if (!(conn = initDbConnection(conn))){
        return -1;
    }

    // Check HID from DB
    snprintf(cmd, CMD_LEN,"select HID from HostMachine where IP = '%s';",ip);
    /* send SQL query */
    if (mysql_query(conn, cmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        return -1;
    }
    res = mysql_use_result(conn);

    if ((row = mysql_fetch_row(res)) != NULL){
        ret = atoi(row[0]);
    }

    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);
    return ret;
}



int registHostMachine(uint8_t *ip)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    char cmd[128];
    int hid;

    // Check HID from DB
    hid = selectHidByIp(ip);
    if (hid > 0){
        return hid;
    }
    
    if (!(conn = initDbConnection(conn))){
        return -1;
    }

    snprintf(cmd, CMD_LEN,"insert into HostMachine (`IP`,`VmNum`) values ('%s','');",ip);
    if (mysql_query(conn,cmd)){
       fprintf(stderr, "%s\n", mysql_error(conn)); 
       return -1;
    }
    res = mysql_use_result(conn);
    hid = selectHidByIp(ip);

    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);
    return hid;
}

int selectVidByIp(uint8_t *ip, int hid)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    MYSQL_ROW   row;
    int         ret = 0;
    char        cmd[CMD_LEN];

    if (!(conn = initDbConnection(conn))){
        return -1;
    }

    // Check HID from DB
    snprintf(cmd, CMD_LEN,"select VID from VirtualMachine where IP = '%s' and Belong = '%d';",ip,hid);
    
    /* send SQL query */
    if (mysql_query(conn, cmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        return -1;
    }
    res = mysql_use_result(conn);

    if ((row = mysql_fetch_row(res)) != NULL){
        ret = atoi(row[0]);
    }

    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);
    return ret;
}

/*
 * INSERT INTO `vngs`.`VirtualMachine` (`VID`, `IP`, `MAC`, `Image`, `Interface`, `FpModule`, `Belong`, `IsBoot`) VALUES (NULL, '10.0.2.5', '52:54:00:12:34:58', '', 'tap0', '', '', '');
 */
int registVirtualMachine(VmStatus *s,int hid,int pid)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    char cmd[CMD_LEN];
    int vid;

    vid = selectVidByIp(s->ip,hid);

    if (vid > 0){
        updateVirtualMachineStatus(s,vid,pid);
        updateVmNumInHost(hid);
        return vid;
    }
    
    if (!(conn = initDbConnection(conn))){
        return -1;
    }

    snprintf(cmd, CMD_LEN,"insert into VirtualMachine (`IP`,`MAC`,`Interface`,`Belong`,`IsBoot`,`pid`) values ('%s','%s','%s','%d','1','%d');",s->ip,s->mac,s->tap,hid,pid);
    if (mysql_query(conn,cmd)){
       fprintf(stderr, "%s\n", mysql_error(conn)); 
       return -1;
    }
    res = mysql_use_result(conn);
    vid = selectVidByIp(s->ip,hid);

    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);

    updateVmNumInHost(hid);

    return vid;
}

int updateVirtualMachineStatus(VmStatus *s, int vid, int pid)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    char cmd[CMD_LEN];
    sprintf(cmd, "update VirtualMachine set MAC='%s', Image='%s',Interface='%s',IsBoot=1,pid=%d where vid=%d;",s->mac,s->img,s->tap,pid,vid);
    if (!(conn = initDbConnection(conn))){
        return -1;
    }
    if (mysql_query(conn,cmd)){
        fprintf(stderr, "%s\n", mysql_error(conn)); 
        return -1;
    }

    res = mysql_use_result(conn);

    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);
    return 1;
}

int vmDown(int vid, int hid)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    char cmd[CMD_LEN];
    sprintf(cmd, "update VirtualMachine set IsBoot=0,pid=0 where vid=%d;",vid);
    if (!(conn = initDbConnection(conn))){
        return -1;
    }
    if (mysql_query(conn,cmd)){
        fprintf(stderr, "%s\n", mysql_error(conn)); 
        return -1;
    }

    res = mysql_use_result(conn);

    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);

    updateVmNumInHost(hid);

    return 1;
}

/*
 *update HostMachine set VmNum=(select count(vid) from VirtualMachine where Belong = 1) where HID = 1
 */

int updateVmNumInHost(int hid)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    char cmd[CMD_LEN];
    sprintf(cmd, "update HostMachine set VmNum=(select count(vid) from VirtualMachine where Belong = %d and IsBoot = 1) where HID = %d",hid,hid);
    if (!(conn = initDbConnection(conn))){
        return -1;
    }
    if (mysql_query(conn,cmd)){
        fprintf(stderr, "%s\n", mysql_error(conn)); 
        return -1;
    }

    res = mysql_use_result(conn);

    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);
    return 1;

}

int insertFpLog(FpStatus *fp, int vid, int hid)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    char        cmd[FPLOG_LEN];

    sprintf(cmd,"insert into FpLog (`Time`,`VID`,`HID`,`Type`,`Action`,`Msg`) values (CURRENT_TIMESTAMP,'%d','%d','%d','%d','%s');",vid,hid,fp->type,fp->action,fp->msg);

    if (!(conn = initDbConnection(conn))){
        return -1;
    }
    if (mysql_query(conn,cmd)){
        fprintf(stderr, "%s\n", mysql_error(conn)); 
        return -1;
    }

    res = mysql_use_result(conn);

    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);
    return 1;
}

int getFpModuleName(int vid, char *name)
{
    MYSQL       *conn = NULL;
    MYSQL_RES   *res;
    MYSQL_ROW   row;
    char        cmd[128];

    if (!(conn = initDbConnection(conn))){
        return -1;
    }

    sprintf(cmd, "select FpModule from VirtualMachine where VID='%d';",vid);

    /* send SQL query */
    if (mysql_query(conn, cmd)) {
        fprintf(stderr, "%s\n", mysql_error(conn));
        return -1;
    }
    res = mysql_use_result(conn);

    if ((row = mysql_fetch_row(res)) != NULL){
        sprintf(name,"%s",row[0]);
    }
    /* close connection */
    mysql_free_result(res);
    mysql_close(conn);

    return 1;
}
