#include "coreApi.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


char *pipeCmd(char *cmd)
{
    FILE *pipe;
    char *outString;
    int  bytes_read;
    size_t  nbytes = PIPE_READ_LEN;
    pipe = popen(cmd,"r");
    if (!pipe){
       fprintf(stderr,"Pipe failed\n"); 
       return NULL;
    }

    outString = malloc(nbytes);
    bytes_read = getdelim(&outString, &nbytes, '\n', pipe);
    if (pclose(pipe) !=0){
       fprintf(stderr,"Could not run '%s', or other error.\n",cmd); 
       return NULL;
    }
    if (bytes_read > 0){
        return outString;
    } else {
        return NULL;
    }
}

int getNetworkStatusByMac(uint8_t *mac, uint8_t *ip, uint8_t *tap)
{
    char macAddr[18] = {0};
    char cmd[PIPE_WRITE_LEN];
    char *ret;

    sprintf(macAddr,"%02x:%02x:%02x:%02x:%02x:%02x",mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
    sprintf(cmd,"awk '{if($4==\"%s\"){print $1, $6}}' /proc/net/arp",macAddr);
    ret = pipeCmd(cmd);
    
    if (ret == NULL){
        return -1;
    }

    sscanf(ret,"%s %s",ip,tap);
    free(ret);
    return 1;
}

int fpDownloader(DeployMsg *fp){

    char cmd[512];

    if (fp->type != DEPLOY){
        return -1;
    } 
    
    sprintf(cmd,"wget -N %s%s -q -P /tmp",fp->path,fp->name);
    printf("%s\n",cmd);
    system(cmd);
    sprintf(cmd,"cp -f /tmp/%s /usr/lib/%s && rm /tmp/%s",fp->name,fp->name,fp->name);
    printf("%s\n",cmd);
    system(cmd);
    return 1;
}
