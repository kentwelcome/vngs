#include <time.h>
#include "dbapi.h"
#include "coreApi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void getNetworkStatusTest()
{
    uint8_t ip[17] = {0};
    uint8_t tap[8] = {0};
    uint8_t phys[6] = {0x52,0x54,0x00,0x12,0x34,0x58};

    getNetworkStatusByMac(phys,ip,tap);
    printf("MAC with IP %s on device %s\n",ip,tap);
}

void getVidFromDbTest()
{
    uint8_t ip[17] = {0};
    memcpy(ip,"10.0.2.5",17);
    int ret;

    ret = selectVidByIp(ip,1);
    printf("Vid of %s is %d\n",ip,ret);
}

void insertVmStatus()
{
    int vid=1;
    VmStatus *vm = malloc(sizeof(VmStatus));
    memcpy(vm->ip,"10.0.2.6",17);
    memcpy(vm->mac,"52:54:00:11:22:33",18);
    memcpy(vm->tap,"tap1",8);

    //vid = registVirtualMachine(vm,4);

    printf("%s is under VM %d\n",vm->ip,vid);
}

void insertFpLogTest()
{
    int vid = 1;
    int hid = 1;
    FpStatus *fp = malloc(sizeof(FpStatus));
    memcpy(fp->msg,"Unit test of the insertFpLog function",64);
    fp->time = time(NULL);
    printf("%d\n",fp->time);
    fp->type = WRITE;
    fp->action = DROP;

    insertFpLog(fp,vid,hid);
}

void deployTest()
{
    char name[64]={0};
    getFpModuleName(4,name);
    printf("Result:%s\n",name);
}

int main()
{
    //char *ip = "140.114.78.142";
    //printf("Regist ip %s with HID:%d\n",ip,registHostMachine(ip));

    //getNetworkStatusTest();

    //getVidFromDbTest();
    //insertVmStatus();

    //insertFpLogTest();

    //printf("Size of VmStatus %d\n",sizeof(VmStatus));
    //printf("Size of FpStatus %d\n",sizeof(FpStatus));

    deployTest();

    return 0;
}
