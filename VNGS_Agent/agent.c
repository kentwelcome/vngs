#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "struct.h"
#include "coreApi.h"


#define C4_PORT 5000
#define C4_IP   "140.114.78.138"
#define MAXCONNECTION  5

#define UINT8_TO_INT(x) (int*)(x)

int listenSocket;
int connectSocket;
struct sockaddr_in c4Server;
struct sockaddr_in agent;

static int shmid;
static FpStatus *shmdata;
static int shmFpId;
static FpName *shmFpName;
static uint8_t nicType;

void wakeupLoop()
{
    /* create socket to listen */
    listenSocket = socket(PF_INET, SOCK_STREAM, 0);

    /* initialize structure agent */
    bzero(&agent, sizeof(agent));
    agent.sin_family = PF_INET;
    agent.sin_port = htons(C4_PORT);
    /* this line is different from client */
    agent.sin_addr.s_addr = INADDR_ANY;

    /* Assign a port number to socket */
    bind(listenSocket, (struct sockaddr*)&agent, sizeof(agent));

    /* make it listen to socket with max 20 connections */
    listen(listenSocket, MAXCONNECTION);

    /* infinity loop -- accepting connection from client forever */
    while(1)
    {
        int clientfd;
        struct sockaddr_in client_addr;
        socklen_t addrlen = sizeof(client_addr);
        char sendBuf[512] = "Hello World\n\0";

        /* Wait and Accept connection */
        clientfd = accept(listenSocket, (struct sockaddr*)&client_addr, &addrlen);

        /* Send message */
        send(clientfd, sendBuf, sizeof(sendBuf), 0);

        /* close(client) */
        close(clientfd);
    }

    /* close(server) , but never get here because of the loop */
    close(listenSocket);
    return;
}

int createConnection()
{
    connectSocket = socket(AF_INET,SOCK_STREAM,0);
    /* initialize value in c4Server */
    bzero(&c4Server, sizeof(c4Server));
    c4Server.sin_family = PF_INET;
    c4Server.sin_port = htons(C4_PORT);
    inet_aton(C4_IP, &c4Server.sin_addr);


    /* Connecting to server */
    return connect(connectSocket, (struct sockaddr*)&c4Server, sizeof(c4Server));
}

void registVmStatus(int signum){
    uint8_t ip[17] = {0};
    uint8_t tap[8] = {0};
    uint8_t *mac   = shmdata->phys;
    VmStatus *vm;
    

    while (1){
        if (getNetworkStatusByMac(mac,ip,tap) > 0){
            printf("IP: %s with device: %s\n",ip,tap);
            break;
        }
        sleep(1);
    }
    /* Save IP to shmdata */
    sscanf((char*)ip,"%d.%d.%d.%d",UINT8_TO_INT(shmdata->ip),UINT8_TO_INT(shmdata->ip+1),UINT8_TO_INT(shmdata->ip+2),UINT8_TO_INT(shmdata->ip+3));
   
    vm = calloc(1,sizeof(VmStatus));
    vm->cmd = VMSTATUS;
    sprintf((char*)vm->mac,"%02x:%02x:%02x:%02x:%02x:%02x",
            mac[0],mac[1],mac[2],mac[3],mac[4],mac[5]);
    memcpy(vm->ip,ip,17);
    memcpy(vm->tap,tap,8);
    send(connectSocket,vm,sizeof(VmStatus),0);
    free(vm);
}

/* Send alert message to c4Server */
void alertFpStatusHandler (int signum)
{
    shmdata->cmd = FPSTATUS;
    send(connectSocket,shmdata,sizeof(FpStatus),0);
}

void *recvLoop(void *argv)
{
    int ret;
    DeployMsg *cmd = malloc(sizeof(DeployMsg));
    while (1){
        ret = recv(connectSocket, cmd, sizeof(DeployMsg), 0);
        if (ret <= 0){
            printf("Close connection\n");
            break;
        }

        if (cmd->type == CONNECTION){
            printf("%s\n",cmd->name);
        } else if (cmd->type == DEPLOY) {
            printf("Get new filtering proceess: %s%s\n",cmd->path,cmd->name);
            fpDownloader(cmd);
            memcpy(shmFpName,cmd->name,sizeof(FpName));
            printf("Send signal to %d with %s\n",getppid(),shmFpName->name);
            kill(getppid(),SIGUSR1);
        } else {
            printf("Unknow Message\n");
        }
    }

    free(cmd);
    pthread_exit(0);
}

int main (int argc, char **argv)
{
    struct  sigaction new_action;
    struct  sigaction vm_action;
    pthread_t tid;
    pid_t pid = getpid();

    if (argc == 2){
        nicType = VIRTIO;
    } else {
        nicType = NE2000;
    }

    /* Set up the structure to specify the new action. */
    new_action.sa_handler = alertFpStatusHandler;
    vm_action.sa_handler = registVmStatus;
    sigemptyset (&new_action.sa_mask);
    sigemptyset (&vm_action.sa_mask);
    new_action.sa_flags = 0;
    vm_action.sa_flags = 0;

    sigaction(SIGUSR1, &new_action, NULL);
    sigaction(SIGUSR2, &vm_action, NULL);

    if (createConnection()){
        perror("Connection fail:");
        return -1;
    }

    shmid = shmget(pid,sizeof(FpStatus),IPC_CREAT);
    shmFpId = shmget(getppid(),sizeof(FpName),IPC_CREAT);
    if (shmid == -1 || shmFpId == -1){
        perror("agent shmget error:");
    }
    shmdata = shmat(shmid,(void*)0,0);
    shmFpName = shmat(shmFpId,(void*)0,0);

    pthread_create(&tid,NULL,&recvLoop,NULL);

    if (nicType == VIRTIO){
        registVmStatus(0);
    }

    pthread_join(tid,NULL);
    shmdt(shmdata);
    shmdt(shmFpName);
    shmctl(shmid,IPC_RMID,0);
    shmctl(shmFpId,IPC_RMID,0);
    close(connectSocket);
    return 0;
}
