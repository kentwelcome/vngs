#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <time.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <arpa/inet.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include "dbapi.h"
#include "coreApi.h"


#define C4_PORT 5000
#define C4_IP   "140.114.78.138"
#define MAXCONNECTION  5

int listenSocket;
int clientfd;
struct sockaddr_in c4Server;
struct sigaction usr1_action;
int vidForDeploy;



int registHostStatus(uint8_t *ip)
{
    return registHostMachine(ip);
}

int registVmStatus(VmStatus *s, int hid, int pid)
{
    int vid = 0;
    printf("VM startup with ip %s\n",s->ip);
    vid = registVirtualMachine(s,hid,pid);
    vidForDeploy = vid;
    return vid;
}

int connectionAccept(int clientfd , uint8_t *ip)
{
    int hid;
    struct sockaddr_in peer;
    socklen_t       peerLen = sizeof(peer);
    if (getpeername(clientfd,(struct sockaddr*)&peer,&peerLen) == -1){
        perror("getpeername failed");
        return -1;
    }
    memcpy(ip,inet_ntoa(peer.sin_addr),strlen(inet_ntoa(peer.sin_addr)));
    printf("Accept connectin from %s\n",ip);
    hid = registHostStatus(ip);
    return hid;
}

void showFpStatus(FpStatus *s)
{
    printf("%08d:\t%s\n",s->time,s->msg);
}


void getDeploySignal()
{
    DeployMsg *cmd = malloc(sizeof(DeployMsg));
    cmd->type = DEPLOY;
    if(getFpModuleName(vidForDeploy,(char*)cmd->name) > 0){
        sprintf((char*)cmd->path,"http://140.114.78.138/~kent/www/fpDir/");
        //sprintf((char*)cmd->name,"test.so");
        send(clientfd,cmd,sizeof(DeployMsg),0);
    }
}



void recvLoop(int clientfd, int hid)
{
    int ret;
    int vid;
    pid_t pid = getpid();
    RecvCmd     *recvData = malloc(sizeof(RecvCmd));
    signal(SIGUSR1, getDeploySignal);
    
    while (1){
        ret = recv(clientfd, recvData, sizeof(RecvCmd), 0);

        /* VM close the connection */
        if (ret <= 0){
            vmDown(vid,hid);
            break;
        }

        if (ret == sizeof(RecvCmd)){
            if (recvData->cmd == FPSTATUS) {
                showFpStatus((FpStatus*)recvData);
                insertFpLog((FpStatus*)recvData,vid,hid);

            } else if (recvData->cmd == VMSTATUS) {
                vid = registVmStatus((VmStatus*)recvData, hid, pid);
            }
        }
    }
    free(recvData);
}

void *listenLoop(void *argv){
    /* infinity loop -- accepting connection from client forever */
    while(1)
    {
        int pid;
        struct sockaddr_in client_addr;
        socklen_t addrlen = sizeof(client_addr);
        uint8_t *ip = malloc(sizeof(uint8_t)*16);

        DeployMsg *cmd = malloc(sizeof(DeployMsg));
        cmd->type = CONNECTION;
        sprintf((char*)cmd->name,"Connection Up");

        /* Wait and Accept connection */
        clientfd = accept(listenSocket, (struct sockaddr*)&client_addr, &addrlen);
        pid = fork();
        if (pid == 0){
            int hid;

            hid = connectionAccept(clientfd,ip);
            /* Send message */
            send(clientfd, cmd, sizeof(DeployMsg), 0);

            /* Revc Loop*/
            recvLoop(clientfd, hid);

            /* close(client) */
            printf("Close connection from %s\n",ip);
            close(clientfd);
            free(ip);
            pthread_exit(0);
        } else {
            printf("Fork PID: %d\n",pid);
        }
    }
}

int main (int argc, char **argv)
{
    pthread_t tid;
    pid_t pid = getpid();

    usr1_action.sa_handler = getDeploySignal;
    sigemptyset(&usr1_action.sa_mask);
    usr1_action.sa_flags = 0;

    sigaction(SIGUSR1, &usr1_action, NULL);

    printf("PID: %d\n",pid);

    /* create socket to listen */
    listenSocket = socket(PF_INET, SOCK_STREAM, 0);

    /* initialize structure agent */
    bzero(&c4Server, sizeof(c4Server));
    c4Server.sin_family = PF_INET;
    c4Server.sin_port = htons(C4_PORT);
    c4Server.sin_addr.s_addr = INADDR_ANY;

    /* Assign a port number to socket */
    if ( bind(listenSocket, (struct sockaddr*)&c4Server, sizeof(c4Server)) ){
        perror("Bind Fail:");
    }

    /* make it listen to socket with max 20 connections */
    listen(listenSocket, MAXCONNECTION);

    pthread_create(&tid, NULL, &listenLoop, NULL);

    pthread_join(tid,NULL);
    /* infinity loop -- accepting connection from client forever */
    
    /*
    while(1)
    {
        int clientfd;
        int pid;
        struct sockaddr_in client_addr;
        socklen_t addrlen = sizeof(client_addr);
        uint8_t buf[512] = "Success connection\n\0";
        uint8_t *ip = malloc(sizeof(uint8_t)*16);

        clientfd = accept(listenSocket, (struct sockaddr*)&client_addr, &addrlen);
        pid = fork();
        if (pid == 0){
            int hid;

            hid = connectionAccept(clientfd,ip);
            send(clientfd, buf, sizeof(buf), 0);

            recvLoop(clientfd, hid);

            printf("Close connection from %s\n",ip);
            close(clientfd);
            free(ip);
            return 1;
        }
    }*/

    /* close(server) , but never get here because of the loop */
    close(listenSocket);
    return 0;
}

